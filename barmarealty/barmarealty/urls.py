from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from barmarealty import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    (r'^localeurl/', include('localeurl.urls')),
    (r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': '/static/images/favicon.ico'}),    
    (r'^$', 'home.views.index'),
    (r'^realty/$', 'realty.views.realty_list'),
    (r'^favorites/$', 'realty.views.realty_list', {'is_favorite': True}),
    (r'^order/$', 'order.views.order'),
    (r'^realty/(?P<slug>[-\w]+)/$', 'realty.views.realty_detail'),
    (r'^search/$', 'search.views.search_realty'),
    (r'^currency/$', 'util.views.set_currency'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
    }),
    (r'^(?P<url>.*)$', 'barmarealty.flatpages.views.extended_flatpage'),
)
