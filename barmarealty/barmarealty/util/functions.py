'''
Created on Apr 22, 2012

@author: nailgun
'''
from django.utils.translation import get_language
def serialize_locations(realty_list):
    """
    Returns [[lat, lang, url],[lat, lang, url], ...]
    E.g. [[39.598281553, -0.383834398438, "/en/realty/realty-2/"], 
            [40.3852589522, -3.88297990625, "/en/realty/realty-4/"]]
    """
    import simplejson as json
    ret_list = []
    for realty in realty_list:
        if realty and realty.geolocation:
            ret_list.append(json.loads('[%s, "/%s/realty/%s/"]' % (realty.geolocation, get_language(), realty.slug)))
    return json.dumps(ret_list)