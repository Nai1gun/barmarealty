from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from currencies.models import Currency
def set_currency(request):
    redirect_to = request.GET.get('next', '/')
    curr = request.GET.get('curr', '')
    if curr == '':
        try:
            del request.session['curr']
        except KeyError:
            pass
    else:
        try:
            currency = Currency.objects.get(code=curr)
            request.session['curr'] = currency
        except ObjectDoesNotExist:
            pass
    return HttpResponseRedirect(redirect_to)