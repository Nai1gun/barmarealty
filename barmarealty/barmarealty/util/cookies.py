'''
Created on Apr 22, 2012

@author: nailgun
'''
import simplejson as json

FAVORITES_COOKIE = 'favorites'

def add_favorite(request, id):
    favorites = get_fav_list(request)

    if id not in favorites:
        favorites.append(id)
    
    return favorites
    
def remove_favorite(request, id):
    favorites = get_fav_list(request)

    if id in favorites:
        favorites.remove(id)
        
    return favorites
        
def get_fav_list(request):
    if request.COOKIES.has_key(FAVORITES_COOKIE):
        favorites = json.loads(request.COOKIES[FAVORITES_COOKIE])
    else:
        favorites = []
    return favorites

def set_fav_list(response, favorites):
    response.set_cookie(FAVORITES_COOKIE, json.dumps(favorites))