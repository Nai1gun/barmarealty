'''

Template tags for the search form.

Created on Mar 21, 2012

@author: nailgun
'''
from django import template
from barmarealty.realty.models import BedroomsCount, BathroomsCount, City,\
    RoomsCount

register = template.Library()

class ObjectsNode(template.Node):
    def __init__(self, context_name, clazz):
        self.context_name = context_name
        self.clazz = clazz        

    def render(self, context):
        objects = self.clazz.objects.all()       

        context[self.context_name] = objects
        return ''
    
@register.tag
def get_rooms_count_values(parser, token):
    """
    Syntax::

        {% get_rooms_count_values as context_name %}

    Example usage::

        {% get_rooms_count_values as rooms %}
    """
    return __get_objects(parser, token, RoomsCount)

@register.tag
def get_bedrooms_count_values(parser, token):
    return __get_objects(parser, token, BedroomsCount)

@register.tag
def get_bathrooms_count_values(parser, token):
    return __get_objects(parser, token, BathroomsCount)

@register.tag
def get_city_values(parser, token):
    return __get_objects(parser, token, City)
    
def __get_objects(parser, token, clazz):
    """
    return all objects of specified Class
    """
    bits = token.split_contents()
    syntax_message = ("%(tag_name)s expects a syntax of %(tag_name)s "
                       " as context_name" %
                       dict(tag_name=bits[0]))
    # Must have 3 bits in the tag
    if len(bits) == 3:
        if bits[1] != 'as':
            raise template.TemplateSyntaxError(syntax_message)
        context_name = bits[2]

        return ObjectsNode(context_name, clazz)
    else:
        raise template.TemplateSyntaxError(syntax_message)