'''
Created on Apr 27, 2012

@author: nailgun
'''
import re
from django.db.models import Max, Min
from barmarealty.realty.models import RoomsCount, BedroomsCount, BathroomsCount,\
    City, Realty
def search(request):
    if not re.search("^/admin/", request.path):
        prices = Realty.objects.aggregate(Min('price_unified'), Max('price_unified'))
        price_min = prices['price_unified__min']
        price_max = prices['price_unified__max']
        try:
            currency = request.session['curr']
            price_min *= currency.factor
            price_max *= currency.factor
        except KeyError:
            pass
        space = Realty.objects.aggregate(Min('total_space'), Max('total_space'))
        return {
            'rooms': RoomsCount.objects.all().order_by('count'),
            'bedrooms': BedroomsCount.objects.all().order_by('count'),
            'bathrooms': BathroomsCount.objects.all().order_by('count'),
            'cities': City.objects.all(),
            'price_min': price_min,
            'price_max': price_max,
            'space_min': space['total_space__min'],
            'space_max': space['total_space__max'],
        }
    else:
        return {}