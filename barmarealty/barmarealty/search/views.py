# -*- coding:utf-8 -*-
from barmarealty.realty.models import Realty
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render_to_response
from django.template.context import RequestContext
import re
from barmarealty.util.functions import serialize_locations
from django.core.exceptions import ObjectDoesNotExist
from currencies.models import Currency
from django.utils.translation import get_language

ITEMS_ON_PAGE = 6
    
def search_realty(request):
    """
    The search function.
    """    
    address = request.GET.get('addr', '')
    city = request.GET.get('city', '')
    price_from = __validate_num(__remove_spaces(request.GET.get('price_from', '')))
    price_to = __validate_num(__remove_spaces(request.GET.get('price_to', '')))
    space_from = __validate_num(__remove_spaces(request.GET.get('space_from', '')))
    space_to = __validate_num(__remove_spaces(request.GET.get('space_to', '')))
    rooms = __validate_count(request.GET.get('room', ''))
    bathrooms = __validate_count(request.GET.get('bath', ''))
    bedrooms = __validate_count(request.GET.get('bed', ''))
    page = request.GET.get('page', 1)
    curr = request.GET.get('curr', '')
    
    price_from_unified = None
    price_to_unified = None
    try:
        currency = Currency.objects.get(code=curr)
        if price_from is not None and price_to is not None:
            price_from_unified = price_from / currency.factor
            price_to_unified = price_to / currency.factor
    except ObjectDoesNotExist:
        pass
    
    realty_list = Realty.objects.filter(show_on_site=True)
    
    if address:
        params = { str("address_%s__icontains" % get_language()): address, }
        realty_list = realty_list.filter(**params)
    if city:
        params = { str("city__name_%s" % get_language()): city, }
        realty_list = realty_list.filter(**params)
    if rooms:
        realty_list = realty_list.filter(rooms_count__count=rooms)
    if bathrooms:
        realty_list = realty_list.filter(bathrooms_count__count=bathrooms)
    if bedrooms:
        realty_list = realty_list.filter(bedrooms_count__count=bedrooms)
    if price_from_unified is not None and price_to_unified is not None:
        realty_list = realty_list.filter(price_unified__range=(price_from_unified, price_to_unified))
    if space_from is not None and space_to is not None:
        realty_list = realty_list.filter(total_space__range=(space_from, space_to))
    
    realty_list = realty_list.order_by('price_unified')
    
    paginator = Paginator(realty_list, ITEMS_ON_PAGE)
        
    try:
        realty_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        realty_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        realty_page = paginator.page(paginator.num_pages)
        
    locations = serialize_locations(realty_page.object_list)

    return render_to_response('search/search.html',
        { 'realty_page': realty_page ,
          'addr': address, 
          'city': city,
          'room': rooms,
          'bath': bathrooms,
          'bed': bedrooms,
          'locations': locations},
          context_instance=RequestContext(request),)

def __remove_spaces(val):
    uniString = val.replace(u"\u00A0", " ")
    return uniString.replace(' ', '')
    
def __validate_count(val):
    try:
        if int(val) > 0:
            return int(val)
        return None
    except ValueError:
        return None
    
def __validate_num(val):
    try:
        return int(val)
    except ValueError:
        return None
    
def __parce_price(val):
    p = re.compile(ur"(?P<from>\d+)€ - (?P<to>\d+)€")
    m = p.match(val)
    if m == None or not m.groups('from') or not m.groups('to'):
        return ('','')
    return m.groups()
    