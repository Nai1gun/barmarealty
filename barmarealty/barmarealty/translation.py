'''
i18n for database models

Created on May 20, 2012

@author: nailgun
'''
from modeltranslation.translator import TranslationOptions, translator
from barmarealty.realty.models import Realty, City, MonetizationCategory, Type,\
    FeatureType, Feature
from barmarealty.flatpages.models import ExtendedFlatPage

class RealtyTranslationOptions(TranslationOptions):
    fields = ('name', 'address', 'description', 'long_description')
    
class CityTranslationOptions(TranslationOptions):
    fields = ('name',)

class MonetizationCategoryTranslationOptions(TranslationOptions):
    fields = ('name',)

class TypeTranslationOptions(TranslationOptions):
    fields = ('name',)
    
class FeatureTypeTranslationOptions(TranslationOptions):
    fields = ('name',)
    
class FeatureTranslationOptions(TranslationOptions):
    fields = ('name',)
    
class ExtendedFlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content', )

translator.register(Realty, RealtyTranslationOptions)
translator.register(City, CityTranslationOptions)
translator.register(MonetizationCategory, MonetizationCategoryTranslationOptions)
translator.register(Type, TypeTranslationOptions)
translator.register(FeatureType, FeatureTypeTranslationOptions)
translator.register(Feature, FeatureTranslationOptions)
translator.register(ExtendedFlatPage, ExtendedFlatPageTranslationOptions)