from django.shortcuts import render_to_response
from barmarealty.home.models import SliderImage
from barmarealty.realty.models import Realty
from django.template.context import RequestContext
from barmarealty.util.functions import serialize_locations
from sets import Set

def index(request):
    
    slider_images = SliderImage.objects.all()
    realty_list = Realty.objects.filter(show_on_site=True, special_offer=True).order_by('-id')[:3]
    try:
        realty_random = Realty.objects.filter(show_on_site=True, special_offer=True).order_by('?')[0]
    except IndexError:
        realty_random = None
        
    if realty_random:
        realty_image = realty_random.first_image()
    else:
        realty_image = None

    relSet = Set(realty_list)
    relSet.add(realty_random)    
    locations = serialize_locations(relSet)
    
    return render_to_response("home/index.html", \
                              { "slider_images" : slider_images , \
                                "realty_list" : realty_list ,
                                "realty" : realty_random,
                                "realty_image" : realty_image,
                                "locations" : locations},
                              context_instance=RequestContext(request),)