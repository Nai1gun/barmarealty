'''
Created on Mar 10, 2012

@author: <a href="mailto:korviakov@gmail.com">Alexander Korvyakov</a>
'''
from barmarealty.home.models import SliderImage
from django.contrib import admin

class SliderImageAdmin(admin.ModelAdmin):
    list_display = ('image_view',)

admin.site.register(SliderImage, SliderImageAdmin)