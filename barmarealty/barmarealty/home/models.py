from django.db import models

class SliderImage(models.Model):
    image = models.ImageField(upload_to='home/images')
    title = models.CharField(max_length=250, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    
    def image_view(self):
        return '<img src="%s"/>' % self.image.url    
    image_view.allow_tags = True
    
    def __unicode__(self):
        return self.image.url.split("/")[-1]

