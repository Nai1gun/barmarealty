from django.shortcuts import render_to_response
from django.template.context import RequestContext
from barmarealty.order.models import OrderForm

def order(request):    
    
    if request.method == 'POST': # If the form has been submitted
        form = OrderForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            form.save()
            # recall is successfully accepted
            order_accepted = True
            form = OrderForm() # unbind form
        else:
            order_accepted = False
            
    else:
        form = OrderForm() # An unbound form
        order_accepted = False
        
    return render_to_response('order/order.html',
        { 'form': form, 'order_accepted': order_accepted },
          context_instance=RequestContext(request),) 