from django.db import models
from barmarealty.realty.models import MonetizationCategory, Type
from django.forms.models import ModelForm
from barmarealty.util.fields import ReCaptchaField

class Order(models.Model):
    """
    The request on realty.
    """
    realty_type = models.ForeignKey(Type)
    monetization_category = models.ForeignKey(MonetizationCategory)
    contact_person = models.CharField(max_length=100)
    phone = models.CharField(max_length=25)
    email = models.EmailField()
    comment = models.TextField()
    
    def __unicode__(self):
        return self.contact_person
    
class OrderForm(ModelForm):
    recaptcha = ReCaptchaField()
    class Meta:
        model = Order