'''
Created on Mar 18, 2012

@author: nailgun
'''
from django.contrib import admin
from barmarealty.order.models import Order

class OrderAdmin(admin.ModelAdmin):
    list_display = ('contact_person', 'email',)
    readonly_fields = ('realty_type', 'monetization_category', 'contact_person', 'phone', 'email', 'comment', )
    def has_add_permission(self, request):
        return False

admin.site.register(Order, OrderAdmin)