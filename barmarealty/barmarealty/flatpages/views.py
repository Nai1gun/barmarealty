from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.conf import settings
from barmarealty.flatpages.models import ExtendedFlatPage
from django.contrib.flatpages.views import render_flatpage

def extended_flatpage(request, url):
    """
    Public interface to the extended flat page view.
    @see flatpages.views.flatpage()
    """
    if not url.endswith('/') and settings.APPEND_SLASH:
        return HttpResponseRedirect("%s/" % request.path)
    if not url.startswith('/'):
        url = "/" + url
    f = get_object_or_404(ExtendedFlatPage, url__exact=url, sites__id__exact=settings.SITE_ID)
    return render_flatpage(request, f)