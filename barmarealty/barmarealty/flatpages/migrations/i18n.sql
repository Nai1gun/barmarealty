-- better to run the add column queries one by one
-- sqlite
-- alter table flatpages_extendedflatpage add "title_en" varchar(200);
-- alter table flatpages_extendedflatpage add "title_ru" varchar(200);
-- alter table flatpages_extendedflatpage add "content_en" text;
-- alter table flatpages_extendedflatpage add "content_ru" text;
-- mysql
alter table flatpages_extendedflatpage add title_en varchar(200);
alter table flatpages_extendedflatpage add title_ru varchar(200);
alter table flatpages_extendedflatpage add content_en text;
alter table flatpages_extendedflatpage add content_ru text;