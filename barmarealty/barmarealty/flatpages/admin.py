'''
Created on Mar 5, 2012

@author: <a href="mailto:korviakov@gmail.com">Alexander Korvyakov</a>
'''
from django.contrib import admin
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django.contrib.flatpages.models import FlatPage

from barmarealty.flatpages.models import ExtendedFlatPage
from modeltranslation.admin import TranslationAdmin

class ExtendedFlatPageForm(FlatpageForm):
    class Meta:
        model = ExtendedFlatPage

class ExtendedFlatPageAdmin(FlatPageAdmin, TranslationAdmin):
    form = ExtendedFlatPageForm
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'sites', 'is_in_menu', 'show_after')}),
    )
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(ExtendedFlatPageAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        self.patch_translation_field(db_field, field, **kwargs)
        return field

admin.site.unregister(FlatPage)
admin.site.register(ExtendedFlatPage, ExtendedFlatPageAdmin)