from django.db import models
from django.contrib.flatpages.models import FlatPage

# Create your models here.

class ExtendedFlatPage(FlatPage):
    is_in_menu = models.BooleanField(default=True)
    show_after= models.ForeignKey("self", \
        null=True, blank=True, default=None, \
        related_name="flatpage_predecessor", \
        help_text="Page that this one should appear after (if any)")
    
    class Meta:
        verbose_name = "Flat page"
    