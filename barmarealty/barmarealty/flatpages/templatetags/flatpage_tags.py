'''
Created on Mar 6, 2012

@author: <a href="mailto:korviakov@gmail.com">Alexander Korvyakov</a>
'''
from django import template
from django.conf import settings
from django.contrib.flatpages.models import FlatPage

register = template.Library()


class FlatpageNode(template.Node):
    def __init__(self, context_name):
        self.context_name = context_name        

    def render(self, context):
        flatpages = FlatPage.objects.filter(sites__id=settings.SITE_ID, 
                                            extendedflatpage__is_in_menu=True) \
            .order_by('extendedflatpage__show_after')        

        context[self.context_name] = flatpages
        return ''

@register.tag
def get_menu_flatpages(parser, token):
    """
    Syntax::

        {% get_menu_flatpages as context_name %}

    Example usage::

        {% get_flatpages as flatpages %}
    """
    bits = token.split_contents()
    syntax_message = ("%(tag_name)s expects a syntax of %(tag_name)s "
                       " as context_name" %
                       dict(tag_name=bits[0]))
    # Must have 3 bits in the tag
    if len(bits) == 3:
        if bits[1] != 'as':
            raise template.TemplateSyntaxError(syntax_message)
        context_name = bits[2]

        return FlatpageNode(context_name)
    else:
        raise template.TemplateSyntaxError(syntax_message)