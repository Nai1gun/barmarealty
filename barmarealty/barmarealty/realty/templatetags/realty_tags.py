'''

Tags to be used in realty templates

Created on Mar 10, 2012

@author: <a href="mailto:korviakov@gmail.com">Alexander Korvyakov</a>
'''
from django import template
from barmarealty.realty.models import MonetizationCategory, Type

register = template.Library()

class MenuTemplateNode(template.Node):
    def __init__(self, context_name, tag_name):
        self.context_name = context_name
        self.tag_name = tag_name        

    def render(self, context):
        ret = []
        if self.tag_name == 'get_categories':
            ret = MonetizationCategory.objects.all()
        else:
            ret = Type.objects.all()
        context[self.context_name] = ret
        return ''    
    
def __get_items(parser, token):
    bits = token.split_contents()
    syntax_message = ("%(tag_name)s expects a syntax of %(tag_name)s "
                       " as context_name" %
                       dict(tag_name=bits[0]))
    # Must have 3 bits in the tag
    if len(bits) == 3:
        if bits[1] != 'as':
            raise template.TemplateSyntaxError(syntax_message)
        context_name = bits[2]
        tag_name=bits[0]

        return MenuTemplateNode(context_name, tag_name)
    else:
        raise template.TemplateSyntaxError(syntax_message)

@register.tag
def get_categories(parser, token):
    return __get_items(parser, token)

@register.tag
def get_types(parser, token):
    return __get_items(parser, token)
