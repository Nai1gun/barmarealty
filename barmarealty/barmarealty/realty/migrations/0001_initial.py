# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'MonetizationCategory'
        db.create_table('realty_monetizationcategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, db_index=True)),
        ))
        db.send_create_signal('realty', ['MonetizationCategory'])

        # Adding model 'City'
        db.create_table('realty_city', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('realty', ['City'])

        # Adding model 'LocationCategory'
        db.create_table('realty_locationcategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal('realty', ['LocationCategory'])

        # Adding model 'LocationTag'
        db.create_table('realty_locationtag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('location_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['realty.LocationCategory'])),
        ))
        db.send_create_signal('realty', ['LocationTag'])

        # Adding model 'RoomsCount'
        db.create_table('realty_roomscount', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('count', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True)),
        ))
        db.send_create_signal('realty', ['RoomsCount'])

        # Adding model 'BedroomsCount'
        db.create_table('realty_bedroomscount', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('count', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True)),
        ))
        db.send_create_signal('realty', ['BedroomsCount'])

        # Adding model 'BathroomsCount'
        db.create_table('realty_bathroomscount', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('count', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True)),
        ))
        db.send_create_signal('realty', ['BathroomsCount'])

        # Adding model 'Type'
        db.create_table('realty_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, db_index=True)),
        ))
        db.send_create_signal('realty', ['Type'])

        # Adding model 'FeatureType'
        db.create_table('realty_featuretype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('realty', ['FeatureType'])

        # Adding model 'Feature'
        db.create_table('realty_feature', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('feature_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='features', to=orm['realty.FeatureType'])),
        ))
        db.send_create_signal('realty', ['Feature'])

        # Adding model 'Realty'
        db.create_table('realty_realty', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, db_index=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('monetization_category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['realty.MonetizationCategory'])),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['realty.City'])),
            ('thumbnail', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('rooms_count', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['realty.RoomsCount'], null=True, blank=True)),
            ('bedrooms_count', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['realty.BedroomsCount'], null=True, blank=True)),
            ('bathrooms_count', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['realty.BathroomsCount'], null=True, blank=True)),
            ('living_space', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('kitchen_space', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('total_space', self.gf('django.db.models.fields.FloatField')()),
            ('floor', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('total_floos', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('realty_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['realty.Type'])),
            ('contact_phone_number', self.gf('django.db.models.fields.CharField')(max_length=25, blank=True)),
            ('price', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['currencies.Currency'])),
            ('special_offer', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('show_on_site', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('geolocation', self.gf('barmarealty.gmap_admin.fields.GeoLocationField')(max_length=100)),
        ))
        db.send_create_signal('realty', ['Realty'])

        # Adding M2M table for field location_tags on 'Realty'
        db.create_table('realty_realty_location_tags', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('realty', models.ForeignKey(orm['realty.realty'], null=False)),
            ('locationtag', models.ForeignKey(orm['realty.locationtag'], null=False))
        ))
        db.create_unique('realty_realty_location_tags', ['realty_id', 'locationtag_id'])

        # Adding M2M table for field features on 'Realty'
        db.create_table('realty_realty_features', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('realty', models.ForeignKey(orm['realty.realty'], null=False)),
            ('feature', models.ForeignKey(orm['realty.feature'], null=False))
        ))
        db.create_unique('realty_realty_features', ['realty_id', 'feature_id'])

        # Adding model 'Image'
        db.create_table('realty_image', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('realty', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['realty.Realty'])),
            ('normal', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal('realty', ['Image'])


    def backwards(self, orm):
        
        # Deleting model 'MonetizationCategory'
        db.delete_table('realty_monetizationcategory')

        # Deleting model 'City'
        db.delete_table('realty_city')

        # Deleting model 'LocationCategory'
        db.delete_table('realty_locationcategory')

        # Deleting model 'LocationTag'
        db.delete_table('realty_locationtag')

        # Deleting model 'RoomsCount'
        db.delete_table('realty_roomscount')

        # Deleting model 'BedroomsCount'
        db.delete_table('realty_bedroomscount')

        # Deleting model 'BathroomsCount'
        db.delete_table('realty_bathroomscount')

        # Deleting model 'Type'
        db.delete_table('realty_type')

        # Deleting model 'FeatureType'
        db.delete_table('realty_featuretype')

        # Deleting model 'Feature'
        db.delete_table('realty_feature')

        # Deleting model 'Realty'
        db.delete_table('realty_realty')

        # Removing M2M table for field location_tags on 'Realty'
        db.delete_table('realty_realty_location_tags')

        # Removing M2M table for field features on 'Realty'
        db.delete_table('realty_realty_features')

        # Deleting model 'Image'
        db.delete_table('realty_image')


    models = {
        'currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'factor': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'realty.bathroomscount': {
            'Meta': {'object_name': 'BathroomsCount'},
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'realty.bedroomscount': {
            'Meta': {'object_name': 'BedroomsCount'},
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'realty.city': {
            'Meta': {'object_name': 'City'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'realty.feature': {
            'Meta': {'object_name': 'Feature'},
            'feature_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'features'", 'to': "orm['realty.FeatureType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'realty.featuretype': {
            'Meta': {'object_name': 'FeatureType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'realty.image': {
            'Meta': {'object_name': 'Image'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'normal': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'realty': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': "orm['realty.Realty']"})
        },
        'realty.locationcategory': {
            'Meta': {'object_name': 'LocationCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        'realty.locationtag': {
            'Meta': {'object_name': 'LocationTag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.LocationCategory']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        'realty.monetizationcategory': {
            'Meta': {'object_name': 'MonetizationCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        },
        'realty.realty': {
            'Meta': {'ordering': "['price']", 'object_name': 'Realty'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'bathrooms_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.BathroomsCount']", 'null': 'True', 'blank': 'True'}),
            'bedrooms_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.BedroomsCount']", 'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.City']"}),
            'contact_phone_number': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['currencies.Currency']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'features': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['realty.Feature']", 'symmetrical': 'False', 'blank': 'True'}),
            'floor': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'geolocation': ('barmarealty.gmap_admin.fields.GeoLocationField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kitchen_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'living_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'location_tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['realty.LocationTag']", 'symmetrical': 'False', 'blank': 'True'}),
            'monetization_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.MonetizationCategory']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'realty_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.Type']"}),
            'rooms_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.RoomsCount']", 'null': 'True', 'blank': 'True'}),
            'show_on_site': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'special_offer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thumbnail': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'total_floos': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'total_space': ('django.db.models.fields.FloatField', [], {})
        },
        'realty.roomscount': {
            'Meta': {'object_name': 'RoomsCount'},
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'realty.type': {
            'Meta': {'object_name': 'Type'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        }
    }

    complete_apps = ['realty']
