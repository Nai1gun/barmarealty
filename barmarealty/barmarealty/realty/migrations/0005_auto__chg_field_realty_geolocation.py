# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Changing field 'Realty.geolocation'
        db.alter_column('realty_realty', 'geolocation', self.gf('barmarealty.gmap_admin.fields.GeoLocationField')(max_length=100, null=True))


    def backwards(self, orm):
        
        # Changing field 'Realty.geolocation'
        db.alter_column('realty_realty', 'geolocation', self.gf('barmarealty.gmap_admin.fields.GeoLocationField')(default='0,0', max_length=100))


    models = {
        'currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'factor': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'realty.bathroomscount': {
            'Meta': {'object_name': 'BathroomsCount'},
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'realty.bedroomscount': {
            'Meta': {'object_name': 'BedroomsCount'},
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'realty.city': {
            'Meta': {'object_name': 'City'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'realty.feature': {
            'Meta': {'object_name': 'Feature'},
            'feature_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'features'", 'to': "orm['realty.FeatureType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'realty.featuretype': {
            'Meta': {'object_name': 'FeatureType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'realty.image': {
            'Meta': {'object_name': 'Image'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'normal': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'realty': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': "orm['realty.Realty']"})
        },
        'realty.locationcategory': {
            'Meta': {'object_name': 'LocationCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        'realty.locationtag': {
            'Meta': {'object_name': 'LocationTag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.LocationCategory']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        'realty.monetizationcategory': {
            'Meta': {'object_name': 'MonetizationCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        },
        'realty.realty': {
            'Meta': {'ordering': "['-id']", 'object_name': 'Realty'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'bathrooms_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.BathroomsCount']", 'null': 'True', 'blank': 'True'}),
            'bedrooms_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.BedroomsCount']", 'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.City']"}),
            'contact_phone_number': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['currencies.Currency']"}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'features': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['realty.Feature']", 'symmetrical': 'False', 'blank': 'True'}),
            'floor': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'geolocation': ('barmarealty.gmap_admin.fields.GeoLocationField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kitchen_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'living_space': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'location_tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['realty.LocationTag']", 'symmetrical': 'False', 'blank': 'True'}),
            'long_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'monetization_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.MonetizationCategory']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'price_unified': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'realty_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.Type']"}),
            'rooms_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['realty.RoomsCount']", 'null': 'True', 'blank': 'True'}),
            'show_on_site': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'special_offer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thumbnail': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'total_floos': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'total_space': ('django.db.models.fields.FloatField', [], {})
        },
        'realty.roomscount': {
            'Meta': {'object_name': 'RoomsCount'},
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'realty.type': {
            'Meta': {'object_name': 'Type'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        }
    }

    complete_apps = ['realty']
