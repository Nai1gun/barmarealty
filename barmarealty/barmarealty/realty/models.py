# -*- coding:utf-8 -*-
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from barmarealty.gmap_admin.fields import GeoLocationField
from currencies.models import Currency
    
class MonetizationCategory(models.Model):
    name = models.CharField(max_length=25)
    slug = models.SlugField(unique=True)
    
    class Meta:
        verbose_name_plural = "Monetization categories"
    
    def __unicode__(self):
        return self.name
    
class City(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "Cities"
            
    def __unicode__(self):
        return self.name
    
class LocationCategory(models.Model):
    name = models.CharField(max_length=25)
    
    class Meta:
        verbose_name_plural = "Location categories"
    
    def __unicode__(self):
        return self.name
    
class LocationTag(models.Model):
    name = models.CharField(max_length=25)
    location_category = models.ForeignKey(LocationCategory)
    
    def __unicode__(self):
        return self.name
    
class RoomsCount(models.Model):
    count = models.PositiveIntegerField(unique=True)
    
    class Meta:
        verbose_name_plural = "Rooms count"
    
    def __unicode__(self):
        return str(self.count)
    
class BedroomsCount(models.Model):
    count = models.PositiveIntegerField(unique=True)
    
    class Meta:
        verbose_name_plural = "Bedrooms count"
    
    def __unicode__(self):
        return str(self.count)
    
class BathroomsCount(models.Model):
    count = models.PositiveIntegerField(unique=True)
    
    class Meta:
        verbose_name_plural = "Bathrooms count"
    
    def __unicode__(self):
        return str(self.count)
    
class Type(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(unique=True)
    
    def __unicode__(self):
        return self.name
    
class FeatureType(models.Model):
    name = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.name    
    
class Feature(models.Model):
    name = models.CharField(max_length=50)
    feature_type = models.ForeignKey(FeatureType, related_name='features')
    
    def __unicode__(self):
        return self.name
    
class Realty(models.Model):
    """
    The class that stores the data that represents realty
    """
    name = models.CharField(max_length=250)
    #the part of the http address - should be unique
    slug = models.SlugField(unique=True)
    address = models.CharField(max_length=250)
    description = models.TextField("Short description", blank=True, null=True)
    long_description = models.TextField(blank=True, null=True)
    monetization_category = models.ForeignKey(MonetizationCategory)
    city = models.ForeignKey(City)
    location_tags = models.ManyToManyField(LocationTag, blank=True)
    thumbnail = ThumbnailerImageField(
        upload_to='realty/images/thumbnail',
        resize_source=dict(size=(185, 104), crop='smart'),
        blank=True, null=True
    )
    rooms_count = models.ForeignKey(RoomsCount, blank=True, null=True)
    bedrooms_count = models.ForeignKey(BedroomsCount, blank=True, null=True)
    bathrooms_count = models.ForeignKey(BathroomsCount, blank=True, null=True)
    living_space = models.PositiveIntegerField(blank=True, null=True)
    kitchen_space = models.PositiveIntegerField(blank=True, null=True)
    total_space = models.FloatField()
    #floor could be "minus"
    floor = models.IntegerField(blank=True, null=True)
    total_floos = models.PositiveIntegerField(blank=True, null=True)
    features = models.ManyToManyField(Feature, blank=True)
    realty_type = models.ForeignKey(Type)
    contact_phone_number = models.CharField(max_length=25, blank=True)
    price = models.PositiveIntegerField()
    price_unified = models.PositiveIntegerField(editable=False)
    currency = models.ForeignKey(Currency)
    special_offer = models.BooleanField(default=False)
    show_on_site = models.BooleanField(default=True)
    geolocation = GeoLocationField(max_length=100, blank=True, null=True) 

    class Meta:
        verbose_name_plural = "Realty"
        ordering = ['-id']
        
    def first_image(self):
        try:
            return self.images.order_by('order_index')[0]
        except IndexError:
            return None
        
    def __unicode__(self):
        return self.name
    
    def save(self):
        self.price_unified = self.price/self.currency.factor
        super(Realty, self).save()

class Image(models.Model):
    
    realty = models.ForeignKey(Realty, related_name='images')
    
    normal = ThumbnailerImageField(
        upload_to='realty/images/normal',
        resize_source=dict(size=(600, 337), crop='smart'),
    )
    
    order_index = models.IntegerField(blank=True, null=True);
    
    def normal_view(self):
        """
        This is a helper thumbnail shown in admin
        """
        thumbnailer = get_thumbnailer(self.normal, relative_name=self.normal.name)
        thumb = thumbnailer.get_thumbnail({'size': (100, 100)})        
            
        return thumb.tag()
       
    normal_view.allow_tags = True
    
    def __unicode__(self):
        return str(self.normal.url.split("/")[-1])
