from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from barmarealty.realty.models import Realty, Feature, Type
from barmarealty.recall.models import RecallForm
from barmarealty.util.cookies import add_favorite, remove_favorite, set_fav_list,\
    get_fav_list
from barmarealty.util.functions import serialize_locations
from sets import Set

ITEMS_ON_PAGE = 6

def realty_list(request, is_favorite = False):    
    cat = request.GET.get('cat', '')
    sort = request.GET.get('sort', '')
    page = request.GET.get('page', 1)
    types = __get_types(request)
    
    realty_list = Realty.objects.filter(show_on_site=True)
    if is_favorite == True:
        realty_list = realty_list.filter(pk__in=get_fav_list(request))
    if cat:
        realty_list = realty_list.filter(monetization_category__slug=cat)
    if types and len(types) > 0:
        realty_list = realty_list.filter(realty_type__slug__in=types)
    if sort:
        realty_list = realty_list.order_by(sort)
    else:
        realty_list = realty_list.order_by('price_unified')
    
    paginator = Paginator(realty_list, ITEMS_ON_PAGE)
        
    try:
        realty_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        realty_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        realty_page = paginator.page(paginator.num_pages)
        
    locations = serialize_locations(realty_page.object_list)
    
    if (types):
        types_param = ','.join(types)
    else:
        types_param = None

    return render_to_response('realty/realty_list.html',
        { 'realty_page': realty_page ,
          'cat': cat, 
          'types': types_param,
          'sort': sort,
          'is_favorite': is_favorite,
          'locations': locations,
          'length': len(realty_list)},
          context_instance=RequestContext(request),)
  
def realty_detail(request, slug):
    
    realty = Realty.objects.get(slug=slug)
    all_features = Feature.objects.all().order_by('feature_type')
    images = realty.images.order_by('order_index')
    favorites = get_fav_list(request)
    
    if request.method == 'POST': # If the form has been submitted
        form = RecallForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            new_recall = form.save(commit=False)
            new_recall.realty = realty
            new_recall.save()
            # recall is successfully accepted
            recall_accepted = True
            form = RecallForm() # unbind form
        else:
            recall_accepted = False
            
    else:
        form = RecallForm() # An unbound form
        recall_accepted = False
        
        favorite = request.GET.get('favorite', '')
        if not favorite == '':
            if favorite == 'true':
                favorites = add_favorite(request, realty.id)
            elif favorite == 'false':
                favorites = remove_favorite(request, realty.id)
                        
    response = render_to_response('realty/realty_detail.html',
        { 'realty': realty,
          'realty_images': images,
          'form': form,
          'recall_accepted': recall_accepted,
          'all_features': all_features,
          'favorites': favorites},
          context_instance=RequestContext(request),)
    
    if request.method == 'GET':
        set_fav_list(response, favorites)        
        
    return response

def __get_types(request):
    type_set= Set()
    all_types = Type.objects.all()
    for type in all_types:
        type_param = request.GET.get(type.slug, None)
        if type_param:
            type_set.add(type_param)
    return type_set
    
    