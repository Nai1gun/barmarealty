'''
Created on Mar 8, 2012

@author: nailgun
'''
from django.contrib import admin
from barmarealty.realty.models import *

class ImageInline(admin.StackedInline):
    model = Image

class RealtyAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    inlines = [ImageInline]
    
class TypeAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    
class MonetizationCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    
class ImageAdmin(admin.ModelAdmin):
    list_display = ('normal_view',)
    def has_add_permission(self, request):
        return False
    
class FeatureAdmin(admin.ModelAdmin):
    list_display = ('name', 'feature_type')

admin.site.register(Realty, RealtyAdmin)
admin.site.register(Type, TypeAdmin)
admin.site.register(MonetizationCategory, MonetizationCategoryAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Feature, FeatureAdmin)
admin.site.register([RoomsCount, BedroomsCount, BathroomsCount, FeatureType, \
                     City, LocationCategory, LocationTag], admin.ModelAdmin)

