'''
Created on Mar 18, 2012

@author: nailgun
'''
from django.contrib import admin
from barmarealty.recall.models import FindMethod, Recall

class RecallAdmin(admin.ModelAdmin):
    list_display = ('contact_person', 'email',)
    readonly_fields = ('contact_person', 'phone',\
        'email', 'city', 'country', 'comment', 'find_method', 'realty')
    def has_add_permission(self, request):
        return False

admin.site.register(Recall, RecallAdmin)
admin.site.register(FindMethod, admin.ModelAdmin)