from django.db import models
from barmarealty.realty.models import Realty
from django.forms.models import ModelForm
from barmarealty.util.fields import ReCaptchaField

class FindMethod(models.Model):
    name = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.name

class Recall(models.Model):
    contact_person = models.CharField(max_length=100)
    phone = models.CharField(max_length=25)
    email = models.EmailField()
    city = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    comment = models.TextField(blank=True, null=True)
    find_method = models.ForeignKey(FindMethod)
    realty = models.ForeignKey(Realty, blank=True, null=True)
    
    def __unicode__(self):
        return self.contact_person    

class RecallForm(ModelForm):    
    recaptcha = ReCaptchaField()
    class Meta:
        model = Recall
        exclude = ('realty',)