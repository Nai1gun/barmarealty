﻿$(document).ready(function() {
	var menuPadding = ($("#menu").width() - $(".menutext").width()) /16;
	$(".menutext a").css("padding-left", menuPadding).css("padding-right", menuPadding);

//	Price range
	var priceSteps = 90;
	var logOfMinPrice = Math.log($("#price-min").val());
	var logOfMaxPrice = Math.log($("#price-max").val());
	var scaleOfPrice = (logOfMaxPrice - logOfMinPrice) / (priceSteps);
	function logprice(position) { return Math.exp(logOfMinPrice + scaleOfPrice*(position));}

	$( "#price-range" ).slider({
		range: true,
		min: 0,
		max: 90,
		step: 1,
		values: [ 0, 90 ],
		slide: function( event, ui ) {
			$( "#price-min" ).val( commafy(Math.round(logprice(ui.values[ 0 ])/1000)*1000));
			$( "#price-max" ).val( commafy(Math.round(logprice(ui.values[ 1 ])/1000)*1000));
		}
	});
	$( "#price-min" ).val( commafy(Math.round(logprice($( "#price-range" ).slider( "values", 0 )))));
	$( "#price-max" ).val( commafy(Math.round(logprice($( "#price-range" ).slider( "values", 1 )))));
	
//	Surface area range
	var surfaceSteps = 90;
	var logOfMinSurface = Math.log($("#surface-min").val());
	var logOfMaxSurface = Math.log($("#surface-max").val());
	var scaleOfSurface = (logOfMaxSurface - logOfMinSurface) / (surfaceSteps);
	function logsurface(position) { return Math.exp(logOfMinSurface + scaleOfSurface*(position));}

	$( "#surface-range" ).slider({
		range: true,
		min: 0,
		max: 90,
		step: 1,
		values: [ 0, 90 ],
		slide: function( event, ui ) {
			$( "#surface-min" ).val( commafy(Math.round(logsurface(ui.values[ 0 ]))));
			$( "#surface-max" ).val( commafy(Math.round(logsurface(ui.values[ 1 ]))));
		}
	});
	$( "#surface-min" ).val( commafy(Math.round(logsurface($( "#surface-range" ).slider( "values", 0 )))));
	$( "#surface-max" ).val( commafy(Math.round(logsurface($( "#surface-range" ).slider( "values", 1 )))));

	function commafy(val) {
    return String(val).split("").reverse().join("")
					  .replace(/(.{3}\B)/g, "$1"+unescape("%A0"))
					  .split("").reverse().join("");
	}
	
});