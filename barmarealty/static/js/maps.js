function show_map(lat, lng) {
	var latLng = new google.maps.LatLng(lat, lng);
    var mapOptions = { zoom: 9, mapTypeId: google.maps.MapTypeId.ROADMAP }
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	$("#map-tab").click(function(){
		google.maps.event.trigger(map, 'resize');
		var marker = new google.maps.Marker({position: latLng});						
		marker.setMap(map);
		map.setCenter(latLng);
	});					
}

var markers = [];

function show_map_wiget(locations) {
	var mapOptions = {
		mapTypeId: google.maps.MapTypeId.ROADMAP, 
		center: center(locations),
		maxZoom: 9
	}
	var bounds = new google.maps.LatLngBounds();
	var map = new google.maps.Map(document.getElementById("map_widget"), mapOptions);
    var i;	
	
    for (i = 0; i < locations.length; i++) {
    	latLng = new google.maps.LatLng(locations[i][0], locations[i][1]); 
		markers[i] = new google.maps.Marker({
			position: latLng,
			map: map,
			url: locations[i][2]
		});
		bounds.extend(latLng);
	}
	map.fitBounds(bounds);
	
	for ( i = 0; i < markers.length; i++ ) {
	    google.maps.event.addListener(markers[i], 'click', function() {
	      window.location.href = this.url;
	    });
	}
}

function center(locations) {
	var sumLat = 0, sumLng = 0;
    for (i = 0; i < locations.length; i++) {  
		sumLat +=locations[i][0];
		sumLng +=locations[i][1];
	}
	var avgLat = sumLat / locations.length;
	var avgLng = sumLng / locations.length; 
	return new google.maps.LatLng(avgLat, avgLng);
}